import metpy
import xarray as xr

from datetime import datetime
from siphon.catalog import TDSCatalog
from xarray.backends import NetCDF4DataStore
from metpy.plots import ContourPlot, MapPanel, PanelContainer
from metpy.units import units

cat = TDSCatalog('https://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_0p5deg/catalog.xml')

ncss = cat.datasets['Best GFS Half Degree Forecast Time Series'].subset()

query = ncss.query()
query.time(datetime(2023,7,10))
query.accept('netcdf')
query.variables('Geopotential_height_isobaric',
                'u-component_of_wind_isobaric',
                'v-component_of_wind_isobaric')
query.add_lonlat()

#somedata = open("Best.nc","r")
print(query)
ds = ncss.get_data(query)
print(ds)

#ds = xr.open_dataset("Best.nc")

ds = xr.open_dataset(NetCDF4DataStore(ds)).metpy.parse_cf()

print(ds)

contour = ContourPlot()
contour.data = ds
contour.field = 'Geopotential_height_isobaric'
contour.level = 850 * units.hPa
contour.linecolor = "Black"
contour.contours = 30

panel = MapPanel()
panel.area = (-135,-60,15,58)
panel.layers = ['coastline','borders','states']
panel.title = 'GFS Map'
panel.plots = [contour]

pc = PanelContainer()
pc.size = (10,8)
pc.panels = [panel]
pc.show()
