import matplotlib.pyplot as plt
import urllib.request
import cartopy.crs as ccrs
import cartopy.feature as cfeature

from metpy.plots import ColdFront, WarmFront, OccludedFront,StationaryFront
from metpy.io import parse_wpc_surface_bulletin
from io import BytesIO

url = 'https://www.wpc.ncep.noaa.gov/discussions/codsus'

with urllib.request.urlopen(url) as response:
    content = response.read()
print(content)

    
df = parse_wpc_surface_bulletin(BytesIO(content))

fig, ax = plt.subplots()
low_strength = df[df['feature'] == 'LOW'].strength
high_strength = df[df['feature'] == 'HIGH'].strength
ax.hist(low_strength,color='tab:red',alpha=0.7,label='Lows')
ax.hist(high_strength,color='tab:blue',alpha=0.7,label='Highs')
ax.legend()
ax.set_xlabel('Pressue [hPa]')
ax.set_ylabel('Count')
plt.title('Current Low and High Strengths')



map_crs = ccrs.LambertConformal(central_latitude=45,central_longitude=-100)

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(1,1,1,projection=map_crs)
ax.set_extent((-135,-65,10,80),crs=ccrs.PlateCarree())

ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.LAND)
ax.add_feature(cfeature.BORDERS)
ax.add_feature(cfeature.STATES)
ax.add_feature(cfeature.LAKES)

features = df[df['feature']=='LOW']
for f in features['geometry']:
    ax.text(f.x, f.y, 'L', transform=ccrs.PlateCarree(), color='red', fontsize=16)

features = df[df['feature']=='HIGH']
for f in features['geometry']:
    ax.text(f.x, f.y, 'H', transform=ccrs.PlateCarree(), color='blue', fontsize=16)

s = 2
feature_names = ['WARM','COLD','STNRY','OCFNT','TROF']
feature_styles = [{'linewidth': 1, 'path_effects': [WarmFront(size=s)]},
                  {'linewidth': 1, 'path_effects': [ColdFront(size=s)]},
                  {'linewidth': 1, 'path_effects': [StationaryFront(size=s)]},
                  {'linewidth': 1, 'path_effects': [OccludedFront(size=s)]},
                  {'linewidth': 2, 'linestyle':'dashed','edgecolor':'darkorange'}]

for name, style in zip(feature_names, feature_styles):
    f = df[df['feature'] == name]
    ax.add_geometries(f.geometry,crs=ccrs.PlateCarree(), **style,facecolor='none')

plt.show()
