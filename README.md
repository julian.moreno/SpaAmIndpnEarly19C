Instructions for building the software in this repository assumes you are using the latest version of Ubuntu. If you are using a different distribution, the installation guides of dependencies are hyperlinked for you to refer to instead.

# Spanish American Wars of Independence in the Early 19th Century
## Primary Research Project
## Contents
 - **SourceDump.** A link and reference guide to all of the sources used, or waiting to be read. I will regularly add any sources that peek my interest as a sort of 'To Read' list. Not all sources listed here may be used in my final article and will not be added to citation.   
 - **WriteUps.** Important information from my readings that require a  summary. They are really just more of a digitization of my handwritten notes. Updates to this file may come irregularly as I will work on my WriteUps mainly at time when I need a break from reading.  
  - **Workspace.** This directory serves a workspace for translating documents and managing statistics. My programs are also stored here.
## Products
The `/SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/FinalPaper` includes the `.tex` file of the fruition of this project. `/SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/FinalPresentation` includes `.tex` file of the slide show presentation I used when I presented my research.
##### [PDFLaTeX](https://pypi.org/project/pdflatex/)
 - This package serves as the most efficient way to go from installation to building your `.tex` files into `.pdf` format
 - To install using pip,
 ```
 pip install pdflatex
 ```
 - Now, to compile the paper into a pdf, first we'll cd into the directory with the file, and then run the simple command as shown below.
 ```
 cd /SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/FinalPaper
 pdflatex Paper.tex
 ```
 - The concept is the same for the slideshow
  ```
 cd /SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/FinalPresentation
 pdflatex SpanAmPres.tex
 ```
## Programs
### **OCR & Translator**
This script was written to be able to take in a .pdf file as input, and output a .txt file with the text from the .pdf file written in the output in a different language.
 - The script is located in   
   `/SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/SAWI-Workspace/translatePdfToTxt.py`
#### Prerequisites
 - Python \>= 3.8
#### Dependencies
##### [OCRmyPDF](https://ocrmypdf.readthedocs.io/en/latest/installation.html)
 ```
 sudo apt install ocrmypdf
 ```
##### [PyPDF2](https://pypi.org/project/PyPDF2/)
 - Using pip  
 ```
 pip install PyPDF2
 ```
##### [Apertium](https://wiki.apertium.org/wiki/Installation)
 ```
 sudo apt install apertium
 ```
 - Now let's get our language library  
   ```
   sudo apt install apertium-es-en
   ```
#### Usage  
 -  Currently, only English and Spanish are supported. When typing the inputs in the command line, use **spa** for Spanish, and **eng** for English.  
 - To use the program on the command line for your PDF, follow this format   
 ```python3 translatePdfToTxt.py {FilePath} {InputLanguage} {OutputLanguage} {StartPage} {EndPage}```
 - Unless if you're ready to wait a while, you should only specify for a few amount of pages to be translated.
 - The FilePath is the location of your .pdf file, the InputLanguage should be the language your PDF file is in. The OutputLanguage is the language that your file will be translated to on the output file. The StartPage marks the starting page that the program will start to read at until the page indicated by EndPage.
 - The output file should be a .txt file, created in the same directory as the script is in. The file's name will be the same as the name of your PDF file with the output language concatanated at the end. For example `GacetaBAJuly.pdf` will become `GacetaBAJuly-eng.txt` if the output language is English.
   - To try this example yourself, we can first enter the workspace directory and then run the program using an example document.   
   ```
   cd SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/SAWI-Workspace
   python3 translatePdfToTxt.py GacetaBA1810.pdf spa eng 14 16
   ```
 - During the OCR process, temporary files are created in the working directory, when the program is complete, they should be automatically removed. Since these temporary files are removed at the end of the process, if you end the script early, you can remove them via commandline with  
 ```
 rm tempf*
 ```
 - Apertium's language libraries are not perfect. Any words that Apertium is not able to translate will be marked with an asterick * at the start of it.
### **Plots**
This program uses Matplotlib to visualize data used in my project. All of the data used are inside the script with a link to their source.
 - The script is located in   
   `/SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/SAWI-Workspace/Plotting.py`
#### Prerequisites
 - Python \>= 3.6
#### Dependencies
##### [Matplotlib](https://matplotlib.org/stable/users/installing/index.html)
 - Using pip   
 ```
 pip install matplotlib
 ```
#### Usage
 - We can enter into the SAWI-Workspace folder and call the program using    
 ```
 cd SpaAmIndpnEarly19C/SpanishAmericanWarsOfIndependence/SAWI-Workspace
 python3 Plotting.py
 ```
 
# Getting a hang of MetPy  
## Side Project   
 MetPy is a powerful package for the visualization of Weather data developed by UniData. It is designed to complement Siphon, a software that allows users to extract information from UniData's servers, allowing users to also access old weather data for case studies.
## Installation
### Prerequisites
 - Python \>= 3.8
### Dependencies
##### [Pandas](https://pandas.pydata.org/docs/getting_started/install.html)
 - Using pip   
 ```
 pip install pandas
 ```
##### [Matplotlib \>= 3.3.0](https://matplotlib.org/stable/users/installing/index.html)
 - Using pip     
 ```
 pip install matplotlib
 ```
 ##### [NumPy \>= 1.18.0](https://numpy.org/install/)
 - Using pip   
 ```
 pip install numpy
 ```
 ##### [XArray \>= 0.18.0](https://docs.xarray.dev/en/stable/getting-started-guide/installing.html)
 - Using pip   
 ```
 pip install xarray
 ```
 - With XArray, we can now check the versions of our packages, in case if we already have some of the required packages installed. We can do this by first entering our python interpreter on the command line with `python3` and then entering the below.
 ```
 python3
 ```
 ```
 import xarray
 xarray.show_versions()
 ```