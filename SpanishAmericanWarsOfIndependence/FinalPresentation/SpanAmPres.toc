\beamer@subsectionintoc {0}{1}{Figure}{2}{0}{0}
\beamer@sectionintoc {1}{Establishment}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Background}{3}{0}{1}
\beamer@sectionintoc {2}{Enlightened Despotism}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Problems}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Bourbon Reforms}{12}{0}{2}
\beamer@sectionintoc {3}{War}{17}{0}{3}
\beamer@subsectionintoc {3}{1}{Early War}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{Vales}{23}{0}{3}
\beamer@subsectionintoc {3}{3}{Good Feelings}{29}{0}{3}
\beamer@subsectionintoc {3}{4}{Succession}{33}{0}{3}
\beamer@subsectionintoc {3}{5}{French Revolution}{39}{0}{3}
\beamer@subsectionintoc {3}{6}{War against France}{43}{0}{3}
\beamer@subsectionintoc {3}{7}{War against Britain}{50}{0}{3}
\beamer@subsectionintoc {3}{8}{Revolution}{62}{0}{3}
\beamer@subsectionintoc {3}{9}{Napoleon}{66}{0}{3}
\beamer@subsectionintoc {3}{10}{Caveats}{70}{0}{3}
\beamer@subsectionintoc {3}{11}{Juntas}{78}{0}{3}
\beamer@subsectionintoc {3}{12}{Restoration}{85}{0}{3}
\beamer@sectionintoc {4}{Referencing}{93}{0}{4}
