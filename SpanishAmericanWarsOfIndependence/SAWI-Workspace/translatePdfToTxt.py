### arg1 FILE : arg2 INPUT LANGUAGE : arg3 OUTPUT LANGUAGE : arg4 START PAGE : arg 5 END PAGE


import os
import sys
import ocrmypdf
import apertium
import PyPDF2

baseLang = sys.argv[2]
outLang = sys.argv[3]

startPage = -1
endPage = -1

output = PyPDF2.PdfWriter()

if(sys.argv[4] != "-a"):
    startPage = int(sys.argv[4])
    endPage = int(sys.argv[5])

translator = apertium.Translator(baseLang,outLang)

###Check if inputs are valid
if os.path.isfile(sys.argv[1]):
    if not(sys.argv[1].endswith('.pdf')):
        print("File is not a pdf")
        exit()
else:
    print("File '"+sys.argv[1]+"' does not exist")
    exit()

if len(sys.argv) != (5 and 6):
    print("missing arguments")
    exit()
inputFile = PyPDF2.PdfReader(sys.argv[1])
if(sys.argv[4] != "-a"):
    if (startPage < 1) and (endPage > inputFile.numPages):
        print("Input pages out of range")
        exit()

### Selecting Pages
if(sys.argv[4] != "-a"):
    for x in range(startPage,endPage):
        p = inputFile.pages[x]
        output.add_page(p)


with open('tempfFreshFile.pdf', 'wb') as f:
    output.write(f)

### Begin Reading the PDF    
print("Reading PDF...")
if(sys.argv[4] != "-a"):
    ocrmypdf.ocr("tempfFreshFile.pdf", 'tempfOCR.pdf',language = baseLang,force_ocr=True,sidecar = 'tempfSidecar.txt')
else:
    ocrmypdf.ocr(sys.argv[1], 'tempfOCR.pdf',langueage = baseLang,redo_ocr = True,sidecar = 'tempfSidecar.txt')

print("opening cache txt")
text = open("tempfSidecar.txt",'r')
textList = text.readlines()

print('Formatting text...')
i = 0
while i < len(textList):
    ourString = textList[i]
    if i+1 >= len(textList):
        break
    if (textList[i+1] != '\n') and (len(ourString) > 1):
        if ourString[-2] == '-':
            textList[i] = ourString.replace('-\n','')
        else:
            textList[i] = ourString.replace('\n', ' ')
    i = i +1
message = ''.join(textList)

### Remove Temporary Files
for file in os.listdir():
    names = {'tempfOCR.pdf', 'tempfSidecar.txt', 'tempfFreshFile.pdf'}
    if file in names:
        os.remove(file)

print('Formatting complete! Now Translating text...')
translatedMessage = translator.translate(message)

### Write new file
if(sys.argv[4] != "-a"):
    with open(sys.argv[1][:-4]+'-'+outLang+'-'+sys.argv[4]+'-'+sys.argv[5]+'.txt','w') as file:
        file.write(translatedMessage)
        print("File saved as '"+sys.argv[1][:-4]+'-'+outLang+'-'+sys.argv[4]+'-'+sys.argv[5]+".txt'")
else:
    with open(sys.argv[1][:-4]+'-'+outLang+'.txt','w') as file:
        file.write(translatedMessage)
        print("File saved as '"+sys.argv[1][:-4]+'-'+outLang+".txt'")
