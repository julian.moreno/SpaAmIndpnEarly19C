import matplotlib.pyplot as plt

caracasLabels = 'Whites','Amerindians','Africans','Mixed','Slaves'
caracasVillageSize = [36358,26246,9628,35775,21818]

# https://www.scielo.br/j/rbepop/a/VbRPdDLN3pj4hJ9hJNhHRmx/?format=pdf&lang=en
# Table 7, Caracas 1778-84



quitoLabels = 'Whites etc.','Amerindians'
quitoVillageSize = [24529,42204]

# https://horizon.documentation.ird.fr/exl-doc/pleins_textes/pleins_textes_4/colloques/26906.pdf
# Table 3: Indian and Non Indian Population of the Audiencia 1780


plt.figure(0)
plt.pie(caracasVillageSize, labels=caracasLabels,autopct='%1.1f%%')
plt.title('Caracas Demographics')

plt.figure(1)
plt.pie(quitoVillageSize, labels=quitoLabels,autopct='%1.1f%%')
plt.title('Quito Demographics')
plt.show()
